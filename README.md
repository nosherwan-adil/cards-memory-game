# Card memory game - Code test

This is a card memory game test Assignment.

## Requirements

Install `yarn`, read how [here](https://yarnpkg.com/en/docs/install).

## Installation

Run the following commands:

1. `git clone git@bitbucket.org:nosherwan-adil/cards-memory-game.git`
2. `cd cards-memory-game `
3. `yarn install`

To start the application you can now run: `yarn start`

## Available commands

- `yarn lint` Run code linting
- `yarn test` Run tests using jest
- `yarn test-coverage` Run tests using jest and display coverage
- `yarn build` Bundle the application
- `yarn start` Run the development environment
- `yarn deploy` Build the application in production mode
- `yarn deploy-windows` Build the application in production mode under Windows

### Make sure you have node version 6 :
```bash
node --version
v6.9.1
```
### Swagger API documentation
[http://localhost:8000/documentation](http://localhost:8000/documentation)

### User Interface
[http://localhost:8080](http://localhost:8080)
