import React from 'react';
import Card from '../Card/Card';

require('./Panel.css');

class App extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			faceUpCard: null,
			deck: [],
		};
		this.shuffleTheDeck = this.shuffleTheDeck.bind(this);
	}
	shuffleTheDeck() {
		const deck = ['0C', '0D', '0H', '0S', '2C', '2D', '2H', '2S', '3C', '3D', '3H', '3S', '4C', '4D', '4H', '4S', '5C', '5D', '5H', '5S', '6C', '6D', '6H', '6S', '7C', '7D', '7H', '7S', '8C', '8D', '8H', '8S', '9C', '9D', '9H', '9S', 'AC', 'AD', 'AH', 'AS', 'JC', 'JD', 'JH', 'JS', 'KC', 'KD', 'KH', 'KS', 'QC', 'QD', 'QH', 'QS'];
		for (let i = deck.length - 1; i > 0; i -= 1) {
			const j = Math.floor(Math.random() * (i + 1));
			[deck[i], deck[j]] = [deck[j], deck[i]];
		}
		this.setState({ deck });
	}
	render() {
		return (
			<div className="panelContainer">
				<h2>Panel</h2>
				<button onClick={this.shuffleTheDeck}>shuffle</button>
				{
					this.state.deck.map(name => <Card name={name} />)
				}
			</div>
		);
	}
}

export default App;
