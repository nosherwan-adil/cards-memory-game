import React from 'react';
import { shallow } from 'enzyme';
import Panel from '../Panel/Panel';

describe('Panel.jsx', () => {
	it('should include "Panel" heading', () => {
		const app = shallow(<Panel />);
		expect(app.contains(<h2>Panel</h2>)).toEqual(true);
	});
});
