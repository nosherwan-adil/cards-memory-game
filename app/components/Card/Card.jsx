import React from 'react';
import PropTypes from 'prop-types';

require('./Card.css');

class App extends React.Component {
	static propTypes = {
		name: PropTypes.string.isRequired,
	};

	constructor(props) {
		super(props);
		this.state = {
			faceUp: false,
			cardUrl: 'https://thehub.se/files/57e8ecf39f6d8b675cc937b9/logo_upload-46e9a2a1a245a83957b2eb1d2551c73d.png',
		};
		this.turnTheCard = this.turnTheCard.bind(this);
	}
	turnTheCard() {
		this.setState({
			faceUp: true,
			cardUrl: `https://deckofcardsapi.com/static/img/${this.props.name}.png`,
		});
	}
	render() {
		return (
			<div className="cardContainer" onClick={this.turnTheCard} role="button" tabIndex="0" >
				<img className="img-responsive" src={this.state.cardUrl} alt="Card" width="100%" />
			</div>
		);
	}
}

export default App;
