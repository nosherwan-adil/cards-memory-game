import React from 'react';
import { shallow } from 'enzyme';
import App from './App';

jest.useFakeTimers();

describe('App.jsx', () => {
	it('should include "DPOrganizer Memory Game" heading', () => {
		const app = shallow(<App myProp="I'm a prop value" />);
		expect(app.contains(<h1>DPOrganizer Memory Game</h1>)).toEqual(true);
	});
});
