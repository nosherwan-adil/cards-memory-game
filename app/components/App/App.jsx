import React from 'react';
import Pannel from '../Panel/Panel';

require('./App.css');

class App extends React.Component {
	static propTypes = {
	};

	constructor(props) {
		super(props);

		this.state = {
			time: new Date(),
		};

		this.timer = null;
	}
	render() {
		return (
			<div>
				<h1>DPOrganizer Memory Game</h1>
				<Pannel />
				<Pannel />
			</div>
		);
	}
}

export default App;
