const Joi = require('joi');
const storageController = require('../controllers/storage-controller');

const externals = {};
externals.list = [
	{
		method: 'GET',
		path: '/service/scores',
		handler: (request, reply) => {
			storageController.getHighScore(reply, request.query.size);
		},
		config: {
			notes: 'Get list of scores',
			tags: ['api'],
			validate: {
				query: {
					size: Joi.number().min(1),
				},
			},
		},
	},
	{
		method: 'POST',
		path: '/service/score',
		handler: (request, reply) => {
			storageController.setScore(request.payload, reply);
		},
		config: {
			notes: 'Add new score',
			tags: ['api'],
			validate: {
				payload: {
					name: Joi.string().required(),
					score: Joi.number().required(),
				},
			},
		},
	},
];

module.exports = externals.list;
