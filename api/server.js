const Hapi = require('hapi');
const ScoreRoutes = require('./routes/score');
const Pack = require('../package');
const Inert = require('inert');
const Vision = require('vision');
const HapiSwagger = require('hapi-swagger');
const HapiCors = require('hapi-cors');

// Create a server with a host and port
const server = new Hapi.Server();
const swaggerOptions = {
	info: {
		title: 'Memory Game API Documentation',
		version: Pack.version,
	},
};
const hapiOptions = {
	origins: ['http://localhost:8080'],
};
server.connection({
	host: 'localhost',
	port: 8000,
});

// Add the route
server.route({
	method: 'GET',
	path: '/',
	handler: (request, reply) => reply('Documentation is <a href="/documentation">here</a>'),
});

server.route({
	method: 'GET',
	path: '/_status',
	handler: (request, reply) => {
		reply({
			message: 'Operational',
			statusCode: 200,
		});
	},
});

server.route(ScoreRoutes);

// Start the server
server.register([
	Inert,
	Vision,
	{
		register: HapiSwagger,
		options: swaggerOptions,
	},
	{
		register: HapiCors,
		options: hapiOptions,
	}], (err) => {
	if (err) {
		throw err;
	}
	server.start((startErr) => {
		if (err) {
			throw startErr;
		}
		console.log(`Server running at: ${server.info.uri}`);
		console.log(`Documentation URL: ${server.info.uri}/documentation`);
	});
});
