const storageController = require('./storage-controller');

test('setScore ', (done) => {
	const socre = { name: 'Adil', score: 10 };
	storageController.setScore(socre, (data) => {
		expect(data).toBe(socre);
		storageController.getHighScore((highscoreList) => {
			expect(highscoreList).toContain(socre);
			done();
		});
	});
});

test('get top Score', (done) => {
	const socre1 = { name: 'Adil', score: 10 };
	const socre2 = { name: 'Elias', score: 8 };
	storageController.setScore(socre1, () => {
		storageController.setScore(socre2, () => {
			storageController.getHighScore((highscoreList) => {
				expect(highscoreList.length).toBe(1);
				expect(highscoreList[0].score).toBe(10);
				done();
			}, 1);
		});
	});
});
