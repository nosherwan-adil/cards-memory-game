const cache = require('memory-cache');

const externals = {};
/**
 * Saves the score into the memory
 * @param {Object} score
 * @param {Function} callback
 */
externals.setScore = (score, callback) => {
	const scores = cache.get('scoresArray');
	if (!scores) {
		cache.put('scoresArray', [score]);
	} else {
		cache.put('scoresArray', [...scores, score]);
	}
	callback(score);
};

/**
 * Returns a list of high score
 * @param {Function} callback
 * @param {Number} size Optional
 */
externals.getHighScore = (callback, size = 10) => {
	const scores = cache.get('scoresArray') || [];
	scores.sort((a, b) => b.score - a.score);
	callback(scores.slice(0, size));
};

module.exports = externals;
